import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { MessagesService } from '../messages.service';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
  unreadCount = 0;
  interval: any;
  loginUserID: any;
  constructor(private router: Router, private storage: Storage, private messagesService: MessagesService) { }
  userName = {
    first_name: '',
    last_name: '',
  }


  ngOnInit() {
    this.storage.get('user').then((val) => {
      this.userName.first_name = val.first_name;
      this.userName.last_name = val.last_name;
      this.loginUserID = val._id;
    })
    this.getUnreadMsgCount();
    this.interval = setInterval(() => {
      this.getUnreadMsgCount();
    }, 5000);
  }

  // ionViewDidLeave() {
  //   clearInterval(this.interval);
  // }

  userData;

  logoutUser() {
    this.storage.remove('token')
    this.storage.remove('user')
    this.storage.remove('role')
    this.router.navigate(['/login'])
  }

  getUnreadMsgCount() {
    this.messagesService.getUnreadMsgCount(this.loginUserID)
      .subscribe(
        (res: any) => {
          if (res.status === 'success') {
            this.unreadCount = +res.data;
          }
        },
        (err) => {
          console.log(err);
        }
      )
  }

}
