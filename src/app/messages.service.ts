import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private addMessage = "http://localhost:3000/api/add_message";
  private getMessage = "http://localhost:3000/api/get_messages";
  private getUnreadMsgCnt = "http://localhost:3000/api/get_unread_count";
  constructor(private http: HttpClient) { }

  getMessages(id) {
    return this.http.get<any>(this.getMessage, {
      params: {
        receiverId: id,
        senderId: '5eb384cad133edf527f0df04'
      }
    })
  }

  getUnreadMsgCount(id) {
    return this.http.get<any>(this.getUnreadMsgCnt, {
      params: {
        id: id
      }
    })
  }

  postMessage(data) {
    return this.http.post<any>(this.addMessage, data)
  }

}
