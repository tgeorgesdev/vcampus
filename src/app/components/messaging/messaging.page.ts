import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { MessagesService } from '../../messages.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.page.html',
  styleUrls: ['./messaging.page.scss'],
})
export class MessagingPage implements OnInit {
  loginUser: any = {};
  messages = []
  nickname = '';
  message = '';
  userData: any;

  receiver: any = {
    "nickname": "landry deuboue",
    "id": "5eb384cad133edf527f0df04",
    "role_type": "Admin"
  };
  interval: any;

  constructor(
    private storage: Storage,
    private toastCtrl: ToastController,
    private messagesService: MessagesService
  ) { }

  ngOnInit() {
    this.storage.get('user').then((val) => {
      this.userData = val;
      this.nickname = this.userData.first_name + ' ' + this.userData.last_name;
      this.loginUser = {
        "id": this.userData._id,
        "role_type": this.userData.role_name,
        "name": this.userData.first_name + ' ' + this.userData.last_name
      };
      this.getMessages();
    });

    this.interval = setInterval(() => {
      this.getMessages();
    }, 5000);
  }

  ionViewDidLeave() {
    clearInterval(this.interval);
  }

  getMessages() {
    this.messagesService.getMessages(this.loginUser.id)
      .subscribe(
        (res: any) => {
          if (res.status === 'success') {
            this.messages = res.data;
          }
        },
        (err) => {
          this.showToast(err.error);
          console.log(err);
        }
      )
  }

  sendMessage() {
    let msgObj = {
      sender: { "nickname": this.nickname, "id": this.loginUser.id, "role_type": this.loginUser.role_type },
      created: (new Date()).toString(),
      text: this.message,
      read_flag: false,
      receiver: { "nickname": this.receiver.nickname, "id": this.receiver.id, "role_type": this.receiver.role_type }
    }
    this.messages.push(
      msgObj
    );
    this.message = "";
    this.messagesService.postMessage(msgObj)
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          this.showToast(err.error);
          console.log(err);
        }
      )
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
